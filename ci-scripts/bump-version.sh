#!/usr/bin/env bash

# Bump versions using semversioner

red="\\e[31m"
reset="\\e[0m"


previous_version=$(semversioner current-version)


semversioner release || { echo -e "${red}✖ New and old versions are the same, you need to add a changeset file. ${reset}"; exit 1; }

new_version=$(semversioner current-version)

echo "Generating CHANGELOG.md file..."
semversioner changelog > CHANGELOG.md

# Use new version in the README.md examples
echo "Replace version '$previous_version' to '$new_version' in README.md ..."
sed -i "s/$previous_version/$new_version/g" README.md

# Use new version in the pipe.yml metadata file
echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
sed -i "s/$previous_version/$new_version/g" setup.py


new_version=$(semversioner current-version)

echo "Generating CHANGELOG.md file..."
semversioner changelog > CHANGELOG.md

echo "Replace version '$previous_version' to '$new_version' in README.md ..."
sed -i "s/:$previous_version/:$new_version/g" README.md

echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
sed -i "s/:$previous_version/:$new_version/g" pipe.yml
